/*
 * File      : application.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2006, RT-Thread Development Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 * 2014-04-27     Bernard      make code cleanup.
 */

#include <board.h>
#include <rtthread.h>

#ifdef RT_USING_LWIP
#include <lwip/sys.h>
#include <lwip/api.h>
#include <netif/ethernetif.h>
#include <lwip/sockets.h> /* 使用BSD Socket接口必須包含sockets.h這個頭文件 */
#include "stm32f4xx_eth.h"
#endif

#include "user_mb_app.h"

#include "gvm.h"

extern State *currentState, *stateHead;
extern Sub_Grafcet *sub_grafcetList, *sub_grafcetHead;

#define THREAD_SYS_MONITOR_PRIO        11
#define THREAD_MBMASTER_POLL_PRIO       9
#define TCP_PORT                     5000

static const char coding_tbl_header[] = "GET /?code=";
static const int  coding_tbl_header_byte = sizeof(coding_tbl_header);

static const char send_data[] = "This is TCP Server from RT-Thread."; /* 發送用到的數據 */
void thread_entry_tcpserv(void* parameter)
{
    /* Coding Table */
    int i;
    uint8 *coding_tbl; 
    uint8 coding_tbl_len;
    /*
    uint8 coding_tbl[] = { 5,
                             0,1,0,3,0,1,0,1,
                             1,1,0,4,0,1,0,2,
                             2,1,0,5,0,1,0,3,
                             3,1,0,7,0,1,0,4,
                             4,1,0,6,0,1,0,0};
    uint8 coding_tbl_len = sizeof(coding_tbl);
    */
    
    /* TCP */
    char *recv_data; /* 用於接收的指針，後面會做一次動態分配以請求可用內存 */
    rt_uint32_t sin_size;
    int sock, connected, bytes_received;
    struct sockaddr_in server_addr, client_addr;
    rt_bool_t stop = RT_FALSE; /* 停止標誌 */
    
    /* LwIP Initialization */
#ifdef RT_USING_LWIP
    {
        extern void lwip_sys_init(void);

        /* register ethernetif device */
        eth_system_device_init();

        rt_hw_stm32_eth_init();

        /* init lwip system */
        lwip_sys_init();
        rt_kprintf("TCP/IP initialized!\n");
        rt_kprintf("TCP Server On\r\n");
    }
#endif

    coding_tbl = (uint8*)rt_malloc(1024);
    coding_tbl_len = 0;
    
    recv_data = rt_malloc(1024); /* 分配接收用的數據緩衝 */
    if (recv_data == RT_NULL)
    {
        rt_kprintf("No memory\n");
        return;
    }

    /* 一個socket在使用前，需要預先創建出來，指定SOCK_STREAM為TCP的socket */
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        /* 創建失敗的錯誤處理 */
        rt_kprintf("Socket error\n"); 

        /* 釋放已分配的接收緩衝 */
        rt_free(recv_data);
        return;
    }

    /* 初始化服務端地址 */
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(TCP_PORT); /* 服務端工作的端口 */
    server_addr.sin_addr.s_addr = INADDR_ANY;
    rt_memset(&(server_addr.sin_zero),8, sizeof(server_addr.sin_zero)); 

    /* 綁定socket到服務端地址 */
    if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
    {
        /* 綁定失敗 */
        rt_kprintf("Unable to bind\n");

        /* 釋放已分配的接收緩衝 */
        rt_free(recv_data);
        return;
    }

    /* 在socket上進行監聽 */
    if (listen(sock, 5) == -1)
    {
        rt_kprintf("Listen error\n");

        /* release recv buffer */
        rt_free(recv_data);
        return;
    }

    // Change IP in rtconfig.h
    rt_kprintf("TCP server ip: %d.%d.%d.%d\n", RT_LWIP_IPADDR0, RT_LWIP_IPADDR1, RT_LWIP_IPADDR2, RT_LWIP_IPADDR3);
    rt_kprintf("\nTCPServer Waiting for client on port: %d\n", TCP_PORT);
    while(stop != RT_TRUE)
    {
        sin_size = sizeof(struct sockaddr_in);

        /* 接受一個客戶端連接socket的請求，這個函數調用是阻塞式的 */
        connected = accept(sock, (struct sockaddr *)&client_addr, &sin_size);
        /* 返回的是連接成功的socket */

        /* 接受返回的client_addr指向了客戶端的地址信息 */
        rt_kprintf("I got a connection from (%s , %d)\n",
                  inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));

       /* 客戶端連接的處理 */
       while (1)
       {
            /* 發送數據到connected socket */
            //send(connected, send_data, strlen(send_data), 0);

            /* 從connected socket中接收數據，接收buffer是1024大小，但並不一定能夠收到1024大小的數據 */
            bytes_received = recv(connected,recv_data, 1024, 0);
            if (bytes_received <= 0)
            {
                /* 接收失敗，關閉這個connected socket */
                lwip_close(connected);
                break;
            }

            /* 有接收到數據，把末端清零 */
            recv_data[bytes_received] = '\0';
            if (strcmp(recv_data , "q") == 0 || strcmp(recv_data , "Q") == 0)
            {
                /* 如果是首字母是q或Q，關閉這個連接 */
                lwip_close(connected);
                break;
            } 
            else if (strcmp(recv_data, "exit") == 0) 
            {
                /* 如果接收的是exit，則關閉整個服務端 */
                lwip_close(connected);
                stop = RT_TRUE;
                break;
            } 
            else 
            {
                /* 在控制終端顯示收到的數據 */
                //rt_kprintf("DATA LEN: %d, RECEIVED DATA = %s \n", bytes_received, recv_data);
                if(rt_memcmp(recv_data, coding_tbl_header, coding_tbl_header_byte - 1) == 0) {
                    rt_kprintf("\nGet coding table \n");
                    coding_tbl_len = 0;
                    for(i = coding_tbl_header_byte; i < bytes_received; i++)
                    {
                        if(recv_data[i] == ' ') break;
                        if(recv_data[i] == '+') continue;
                        coding_tbl[coding_tbl_len++] = recv_data[i] - '0';
                        rt_kprintf("%c ", recv_data[i]);
                    }
                    coding_tbl[coding_tbl_len] = '\0';
                    rt_kprintf("\nSize = %d\n", coding_tbl_len);
                    release();
                    parser(coding_tbl, coding_tbl_len);
                    currentState = stateHead;
                    rt_kprintf("Run coding table\n");
                    rt_kprintf("===============\n");
                    /* Proecess coding table */
                    while(currentState != 0)
                    {
                        processor();
                        currentState = currentState->nextState;
                        // Muset delay here
                        rt_thread_delay(1000);
                    }
                    rt_kprintf("===============\n");
                } 
            }       
        }
    }
}

void thread_entry_mbmaster_poll(void* parameter)
{
    eMBMasterInit(MB_RTU, 3, 115200,  MB_PAR_NONE);
    eMBMasterEnable();
    rt_kprintf("Modbus initialized!\n");
    while (1) {
        eMBMasterPoll();
    }
}

void rt_init_thread_entry(void* parameter)
{
    /* LwIP Initialization */
#ifdef RT_USING_LWIP
    {
        extern void lwip_sys_init(void);

        /* register ethernetif device */
        eth_system_device_init();

        rt_hw_stm32_eth_init();

        /* init lwip system */
        lwip_sys_init();
        rt_kprintf("TCP/IP initialized!\n");
    }
#endif
}

int rt_application_init()
{
    
    rt_thread_t tid;
    /*
    tid = rt_thread_create("init",
        rt_init_thread_entry, RT_NULL,
        2048, THREAD_SYS_INIT_PRIO, 100);
    if (tid != RT_NULL) rt_thread_startup(tid);
    */
    tid = rt_thread_create("tcpserv",
        thread_entry_tcpserv, RT_NULL,
        4096, THREAD_SYS_MONITOR_PRIO, 5);
    if (tid != RT_NULL) rt_thread_startup(tid);
    
    tid = rt_thread_create("mbmaster_poll",
        thread_entry_mbmaster_poll, RT_NULL,
        4096, THREAD_MBMASTER_POLL_PRIO, 5);
    if (tid != RT_NULL) rt_thread_startup(tid);
    
    return 0;
}

/*@}*/
