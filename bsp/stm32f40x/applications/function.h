#ifndef _FUNCTION_H_
#define _FUNCTION_H_

#include "data_type.h"

typedef struct
{
    uint8 functionNumber;
    uint8 parameterNumber;
    void* function;
} FunctionTable;

void DisplayToWebPage(void);
void GetTemperature(void);
void GetHumidity(void);
void GetIllumination(void);
void GetRollAngle(void);
void GetPitchAngle(void);
void getSlaveData(uint8 slaveAddr, uint16 regAddr, uint16 regCount);

#endif
