#ifndef _GVM_H_
#define _GVM_H_

#include "data_type.h"

typedef struct ActionNode
{
    uint8 returnVariable;
    uint8 functionNumber;
    uint32 *functionParameter;
    struct ActionNode *nextAction;
} Action;

typedef struct Sub_GrafcetNode
{
    uint8 sub_grafcetNumber;
    uint8 *codingTable;
    uint32 codingTableLength;
    struct Sub_GrafcetNode *nextSub_Grafcet;
} Sub_Grafcet;

typedef struct OperandNode
{
    uint8 compareVariable;
    uint8 compareOperator;
    uint32 comparedValue;
} Operand;

typedef struct ConditionNode
{
    uint8 logicalOperator;
    struct OperandNode *operand;
    struct ConditionNode *nextCondition;
} Condition;

typedef struct TransitionNode
{
    uint8 conditionType;
    struct ConditionNode *condition;
    struct StateNode *toState;
    struct TransitionNode *nextTransition;
} Transition;

typedef struct StateNode
{
    uint32 stateNumber;
    struct ActionNode *action;
    struct Sub_GrafcetNode *sub_grafcet;
    struct TransitionNode *transition;
    struct StateNode *nextState;
} State;

typedef void* (*func0)(void);
typedef void* (*func1)(uint32);
typedef void* (*func2)(uint32, uint32);
typedef void* (*func3)(uint32, uint32, uint32);
typedef void* (*func4)(uint32, uint32, uint32, uint32);

typedef enum
{
    CMPOP_NONE,
    CMPOP_GT,
    CMPOP_GE,
    CMPOP_LT,
    CMPOP_LE,
    CMPOP_EQ,
    CMPOP_NE,
} CMPOP;

typedef enum
{
    LOGICOP_NONE,
    LOGICOP_OR,
    LOGICOP_AND
} LOGICOP;

void parser(uint8 *codingTable, uint8 codingTableLen);

void release(void);

void processor(void);

#endif
