#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <rtthread.h>

#include "gvm.h"
#include "function.h"

extern int a, b, c, d, e, f;
extern FunctionTable funcTable[];
extern int funcTableSize;

State *stateHead = NULL, *currentState = NULL, *temp = NULL;
Sub_Grafcet *sub_grafcetList, *sub_grafcetHead;

// ²£¥Í Action Node
Action* newAction(uint8 returnVar, uint8 funcNum, uint8 paramNum, uint32 *funcParam)
{
    Action *newNode = (Action *)malloc(sizeof(Action));
    newNode->returnVariable = returnVar;
    newNode->functionNumber = funcNum;
    newNode->functionParameter = (uint32 *)malloc(sizeof(uint32)*paramNum);
    memcpy(newNode->functionParameter, funcParam, sizeof(uint32)*paramNum);
    newNode->nextAction = NULL;
    return newNode;
}

Sub_Grafcet* newSub_Grafcet(uint8 sub_grafcetNum, uint8* codingTable, uint32 codingTableLen)
{
    Sub_Grafcet *newNode = (Sub_Grafcet *)malloc(sizeof(Sub_Grafcet));
    newNode->sub_grafcetNumber = sub_grafcetNum;
    newNode->codingTableLength = codingTableLen;
    newNode->codingTable = (uint8 *)malloc(sizeof(uint8)*codingTableLen);
    memcpy(newNode->codingTable, codingTable, sizeof(uint8)*codingTableLen);
    newNode->nextSub_Grafcet = NULL;
    return newNode;
}

// ²£¥Í Operand Node
Operand* newOperand(uint8 cmpVar, uint8 cmpOp, uint32 cmpVal)
{
    Operand *newNode = (Operand *)malloc(sizeof(Operand));
    newNode->compareVariable = cmpVar;
    newNode->compareOperator = cmpOp;
    newNode->comparedValue = cmpVal;
    return newNode;
}

Condition* newCondition(uint8 logicOp, Operand* operand)
{
    Condition *newNode = (Condition *)malloc(sizeof(Condition));
    newNode->logicalOperator = logicOp;
    newNode->operand = operand;
    newNode->nextCondition = NULL;
    return newNode;
}

Transition* newTransition(uint8 conditionType)
{
    Transition *newNode = (Transition *)malloc(sizeof(Transition));
    newNode->conditionType = conditionType;
    newNode->condition = NULL;
    newNode->toState = NULL;
    newNode->nextTransition = NULL;
    return newNode;
}

State* newState(uint32 stateNum)
{
    State *newNode = (State *)malloc(sizeof(State));
    newNode->stateNumber = stateNum;
    newNode->action = NULL;
    newNode->transition = NULL;
    newNode->nextState = NULL;
    return newNode;
}

void parser(uint8 *codingTable, uint8 codingTableLen)
{
    int i, j;
    uint32 index = 0;
    uint8 stateNum, actionNum, transitionNum, conditionNum;
    uint8 fromStatNum, toStateNum;
    uint8 returnVar, funcNum, paramNum;
    uint32 funcParam[4];
    uint8 conditionType;
    uint8 cmpVar, cmpOp, logicOp;
    uint32 cmpVal;
    State *stateList, *fromState, *toState;
    Action *actionList;
    Transition *transitionList;
    Condition *conditionList;
    Operand *operand;


    stateNum = codingTable[index++];
    //rt_kprintf("State number: %d\n", stateNum);
    // creat state list
    
    for(i = 0; i < stateNum; i++)
    {
        if(i == 0)
        {
            stateList = newState(0);
            stateHead = stateList;
        }
        else
        {
            stateList->nextState = newState(i);
            stateList = stateList->nextState;
        }
        //rt_kprintf("[%d] state: %p\n", i,  stateList);

    }
    
    while(index != codingTableLen)
    {
        // search fromState
        stateList = stateHead;
        fromStatNum = codingTable[index++];
        while(stateList != NULL)
        {
            if(stateList->stateNumber == fromStatNum)
            {
                fromState = stateList;
                break;
            }
            stateList = stateList->nextState;
        }

        if(codingTable[index] == 48)
        {
            index++;
            sub_grafcetList = sub_grafcetHead;
            while(sub_grafcetList != NULL)
            {
                if(sub_grafcetList->sub_grafcetNumber == codingTable[index])
                {
                    fromState->sub_grafcet = sub_grafcetList;
                    index++;
                    break;
                }
                sub_grafcetList = sub_grafcetList->nextSub_Grafcet;
            }
        }
        else
        {
            actionNum = codingTable[index++];
            
            //rt_kprintf("====================\n");
            //rt_kprintf("actionNum: %d\n", actionNum);
            // creat fromState action list
            for(i = 0; i < actionNum; i++)
            {
                returnVar = codingTable[index++];
                funcNum = codingTable[index++];
                paramNum = codingTable[index++];
                for(j = 0; j < paramNum; j++)
                {
                    funcParam[j] = (uint32)codingTable[index++]<<24;
                    funcParam[j] |= (uint32)codingTable[index++]<<16;
                    funcParam[j] |= (uint32)codingTable[index++]<<8;
                    funcParam[j] |= (uint32)codingTable[index++];
                }
                if(i == 0)
                {
                    fromState->action = newAction(returnVar, funcNum, paramNum, funcParam);
                    actionList = fromState->action;
                }
                else
                {
                    actionList->nextAction = newAction(returnVar, funcNum, paramNum, funcParam);
                    actionList = actionList->nextAction;
                }

                //rt_kprintf("[%d] Action: %p\n", i, actionList);
                //rt_kprintf("  returnVar: %d\n", returnVar);
                //rt_kprintf("  funcNum: %d\n", funcNum);
                //rt_kprintf("  paramNum: %d\n", paramNum);
                //rt_kprintf("  next: %d\n", actionList->nextAction);

            }
        }

        transitionNum = codingTable[index++];
        //rt_kprintf("transitionNum: %d\n", transitionNum);
        for(i = 0; i < transitionNum; i++)
        {
            conditionType = codingTable[index++];
            if(i == 0)
            {
                fromState->transition = newTransition(conditionType);
                transitionList = fromState->transition;
            }
            else
            {
                transitionList->nextTransition = newTransition(conditionType);
                transitionList = transitionList->nextTransition;
            }
            // assign fromState transition condition
            if(conditionType == 2)
            {
                conditionNum = codingTable[index++];
                for(j = 0; j < conditionNum; j++)
                {
                    if(codingTable[index] >= 10 && codingTable[index] <= 15)
                    {
                        cmpVar = codingTable[index++];
                        cmpOp = codingTable[index++];
                        cmpVal = (uint32)codingTable[index++]<<24;
                        cmpVal |= (uint32)codingTable[index++]<<16;
                        cmpVal |= (uint32)codingTable[index++]<<8;
                        cmpVal |= (uint32)codingTable[index++];
                        operand = newOperand(cmpVar, cmpOp, cmpVal);
                        logicOp = NULL;
                    }
                    else
                    {
                        operand = NULL;
                        logicOp = codingTable[index++];
                    }
                    if(j == 0)
                    {
                        transitionList->condition = newCondition(logicOp, operand);
                        conditionList = transitionList->condition;
                    }
                    else
                    {
                        conditionList->nextCondition = newCondition(logicOp, operand);
                        conditionList = conditionList->nextCondition;
                    }
                }
            }

            stateList = stateHead;
            toStateNum = codingTable[index++];
            // search toState
            while(stateList != NULL)
            {
                if(stateList->stateNumber == toStateNum)
                {
                    toState = stateList;
                    break;
                }
                stateList = stateList->nextState;
            }
            transitionList->toState = toState;
        }

    }
    
}

void release(void)
{
    State *stateList = stateHead, *stateTemp;
    Action *actionList, *actionTemp;
    Transition *transitionList, *transitionTemp;
    Condition *conditionList, *conditionTemp;

    while(stateList != NULL)
    {
        actionList = stateList->action;
        while(actionList != NULL)
        {
            free(actionList->functionParameter);

            actionTemp = actionList;
            free(actionTemp);
            actionList = actionList->nextAction;
        }

        transitionList = stateList->transition;
        while(transitionList != NULL)
        {
            conditionList = transitionList->condition;
            while(conditionList != NULL)
            {
                if(conditionList->operand != NULL)
                    free(conditionList->operand);

                conditionTemp = conditionList;
                free(conditionTemp);
                conditionList = conditionList->nextCondition;
            }

            transitionTemp = transitionList;
            free(transitionTemp);
            transitionList = transitionList->nextTransition;
        }

        stateTemp = stateList;
        free(stateTemp);
        stateList = stateList->nextState;
    }
}

void* doFunction(uint8 funcNum, uint32 *funcParam)
{
    int i;
    void *returnValue;
    func0 f0;
    func1 f1;
    func2 f2;
    func3 f3;
    func4 f4;

    for(i = 0; i < funcTableSize; i++)
    {
        // check function number
        if(funcNum == funcTable[i].functionNumber)
        {
            switch(funcTable[i].parameterNumber)
            {
            case 0:
                f0 = (func0)funcTable[i].function;
                returnValue = f0();
                break;
            case 1:
                f1 = (func1)funcTable[i].function;
                returnValue = f1(funcParam[0]);
                break;
            case 2:
                f2 = (func2)funcTable[i].function;
                returnValue = f2(funcParam[0], funcParam[1]);
                break;
            case 3:
                f3 = (func3)funcTable[i].function;
                returnValue = f3(funcParam[0], funcParam[1], funcParam[2]);
                break;
            case 4:
                f4 = (func4)funcTable[i].function;
                returnValue = f4(funcParam[0], funcParam[1], funcParam[2], funcParam[3]);
                break;
            }
        }
    }
    return returnValue;
}

void doAction(Action *currentAction)
{
    while(currentAction != NULL)
    {
        //rt_kprintf("Action: %p\n", currentAction);
        //rt_kprintf("  returnVar: %d\n", currentAction->returnVariable);
        //rt_kprintf("  funcNum: %d\n", currentAction->functionNumber);
        //rt_kprintf("  paramNum: %p\n", currentAction->nextAction);
        switch(currentAction->returnVariable)
        {
        case 10:
            a = (int)doFunction(currentAction->functionNumber, currentAction->functionParameter);
            break;
        case 11:
            b = (int)doFunction(currentAction->functionNumber, currentAction->functionParameter);
            break;
        case 12:
            c = (int)doFunction(currentAction->functionNumber, currentAction->functionParameter);
            break;
        case 13:
            d = (int)doFunction(currentAction->functionNumber, currentAction->functionParameter);
            break;
        case 14:
            e = (int)doFunction(currentAction->functionNumber, currentAction->functionParameter);
            break;
        case 15:
            f = (int)doFunction(currentAction->functionNumber, currentAction->functionParameter);
            break;
        default:
            doFunction(currentAction->functionNumber, currentAction->functionParameter);
        }
        currentAction = currentAction->nextAction;
    }
}

void doSub_Grafcet(Sub_Grafcet *currentSub_Grafcet)
{
    if(currentSub_Grafcet != NULL)
    {
        //        ucSlaveAddress = searchSlaveID(currentSub_Grafcet->codingTable, currentSub_Grafcet->codingTableLength);
        //        if(ucSlaveAddress != 0)
        {
            //            SlaveMsg[ucSlaveAddress].work = 1;
            //            writeRegFrame(currentSub_Grafcet->codingTable, currentSub_Grafcet->codingTableLength);
            //            xMBPortEventPost(EV_WRITE_REGISTER);
            //            eMBMaster();
            rt_kprintf("Send coding table to slave\r\n");
            //        }
            //        else
            //        {
            rt_kprintf("No more this type slave\r\n");
        }
    }
}

int getVariable(uint8 var)
{
    switch(var)
    {
    case 10:
        return a;
    case 11:
        return b;
    case 12:
        return c;
    case 13:
        return d;
    case 14:
        return e;
    case 15:
        return f;
    }
    return 0;
}
boolean judgeCondition(Operand *operand)
{
    int compareVariable = getVariable(operand->compareVariable);
    int comparedValue = operand->comparedValue;
    switch(operand->compareOperator)
    {
    case CMPOP_GT:
        return (compareVariable > comparedValue);
    case CMPOP_GE:
        return (compareVariable >= comparedValue);
    case CMPOP_LT:
        return (compareVariable < comparedValue);
    case CMPOP_LE:
        return (compareVariable <= comparedValue);
    case CMPOP_EQ:
        return (compareVariable == comparedValue);
    case CMPOP_NE:
        return (compareVariable != comparedValue);
    default:
        return TRUE;
    }
}

void doTransition(Transition *currentTransition)
{
    boolean pass;
    uint8 stack[20], top;
    Condition *currentCondition;
    while(currentTransition != NULL)
    {
        currentCondition = currentTransition->condition;
        top = 0;
        if(currentTransition->conditionType == 0)
            stack[top] = FALSE;
        else
            stack[top] = TRUE;
        while(currentCondition != NULL)
        {
            switch(currentCondition->logicalOperator)
            {
            case LOGICOP_OR:	// ¹J¨ì Logical Operator ´N ±q stack pop ¥X¨â­Ó°µOperator
                pass = stack[--top];
                pass |= stack[--top];
                stack[top++] = pass;
                break;
            case LOGICOP_AND:
                pass = stack[--top];
                pass &= stack[--top];
                stack[top++] = pass;
                break;
            default:	//
                stack[top++] = judgeCondition(currentCondition->operand);
                break;
            }
            currentCondition = currentCondition->nextCondition;
        }
        if(stack[0])
        {
            currentState = currentTransition->toState;
            break;
        }
        currentTransition = currentTransition->nextTransition;
    }
}

void processor(void)
{
    doAction(currentState->action);
    if(currentState != NULL)
    {
        doSub_Grafcet(currentState->sub_grafcet);
        doTransition(currentState->transition);
    }
}
