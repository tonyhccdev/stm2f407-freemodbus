#include <stdint.h>
#include <stdio.h>

#include <rtthread.h>
#include "user_mb_app.h"

extern uint16_t   ucSDiscInBuf[]  ;
extern uint16_t   ucSCoilBuf[]    ;
extern uint16_t   usSRegInBuf[]   ;
extern uint16_t   usSRegHoldBuf[] ;


#include "gvm.h"
#include "function.h"

int a, b, c, d, e, f;
extern State *currentState, *temp;

FunctionTable funcTable[] =
{
    {0,		0,	(void*)DisplayToWebPage},
    {3,		0,	(void*)GetTemperature},
    {4,		0,	(void*)GetHumidity},
    {5,		0,	(void*)GetIllumination},
    {6,		0,	(void*)GetRollAngle},
    {7,		0,	(void*)GetPitchAngle}
};

int funcTableSize = sizeof(funcTable)/sizeof(FunctionTable);


void DisplayToWebPage(void)
{
    temp = currentState->transition->toState;
    currentState = NULL;
}

void GetTemperature(void)
{
    UCHAR sndAddr = 1;
    USHORT regAddr = 0;
    USHORT regData = rt_tick_get() & 0xFF;
    rt_kprintf("GetTemperature\n"); 
    rt_kprintf("SLAVE: %2d, REG ADDR: %2d, DATA: %02X\n", sndAddr, regAddr, regData); 
    eMBMasterReqWriteHoldingRegister(sndAddr, regAddr, regData, RT_WAITING_FOREVER);

}

void GetHumidity(void)
{
    UCHAR sndAddr = 1;
    USHORT regAddr = 2;
    USHORT regData = rt_tick_get() & 0xFFFF;
    rt_kprintf("GetHumidity\n"); 
    rt_kprintf("SLAVE: %2d, REG ADDR: %2d, DATA: %02X\n", sndAddr, regAddr, regData); 
    eMBMasterReqWriteHoldingRegister(sndAddr, regAddr, regData, RT_WAITING_FOREVER);
}

void GetIllumination(void)
{
    UCHAR sndAddr = 1;
    USHORT regAddr = 4;
    USHORT regData = rt_tick_get() & 0xFFFF;
    rt_kprintf("GetIllumination\n");
    rt_kprintf("SLAVE: %2d, REG ADDR: %2d, DATA: %02X\n", sndAddr, regAddr, regData); 
    eMBMasterReqWriteHoldingRegister(sndAddr, regAddr, regData, RT_WAITING_FOREVER);
}

void GetRollAngle(void)
{
    UCHAR sndAddr = 1;
    USHORT regAddr = 6;
    USHORT regData = rt_tick_get() & 0xFFFF;
    rt_kprintf("GetRollAngle\n");
    rt_kprintf("SLAVE: %2d, REG ADDR: %2d, DATA: %02X\n", sndAddr, regAddr, regData); 
    eMBMasterReqWriteHoldingRegister(sndAddr, regAddr, regData, RT_WAITING_FOREVER);
}

void GetPitchAngle(void)
{
    UCHAR sndAddr = 1;
    USHORT regAddr = 8;
    USHORT regData = rt_tick_get() & 0xFFFF;
    rt_kprintf("GetPitchAngle\n");
    rt_kprintf("SLAVE: %2d, REG ADDR: %2d, DATA: %02X\n", sndAddr, regAddr, regData); 
    eMBMasterReqWriteHoldingRegister(sndAddr, regAddr, regData, RT_WAITING_FOREVER);
}
