#ifndef _DATA_TYPE_H_
#define _DATA_TYPE_H_

#define TRUE 1
#define FALSE 0

typedef signed char    int8;
typedef signed short   int16;
typedef signed long    int32;

typedef unsigned char  boolean;
typedef unsigned char  uint8;
typedef unsigned short uint16;
typedef unsigned long  uint32;

#endif // _DATA_TYPE_H_
