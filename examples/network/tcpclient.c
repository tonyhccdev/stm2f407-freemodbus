#include <rtthread.h>
#include <lwip/netdb.h> /* 為瞭解析主機名，需要包含netdb.h頭文件 */
#include <lwip/sockets.h> /* 使用BSD socket，需要包含sockets.h頭文件 */

#define BUFSZ	1024

static const char send_data[] = "This is TCP Client from RT-Thread."; /* 發送用到的數據 */
void tcpclient(const char* url, int port)
{
    char *recv_data;
    struct hostent *host;
    int sock, bytes_received;
    struct sockaddr_in server_addr;

    /* 通過函數入口參數url獲得host地址（如果是域名，會做域名解析） */
    host = gethostbyname(url);

    /* 分配用於存放接收數據的緩衝 */
    recv_data = rt_malloc(BUFSZ);
    if (recv_data == RT_NULL)
    {
        rt_kprintf("No memory\n");
        return;
    }

    /* 創建一個socket，類型是SOCKET_STREAM，TCP類型 */
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        /* 創建socket失敗 */
        rt_kprintf("Socket error\n");

        /* 釋放接收緩衝 */
        rt_free(recv_data);
        return;
    }

    /* 初始化預連接的服務端地址 */
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr = *((struct in_addr *)host->h_addr);
    rt_memset(&(server_addr.sin_zero), 0, sizeof(server_addr.sin_zero));

    /* 連接到服務端 */
    if (connect(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
    {
        /* 連接失敗 */
        rt_kprintf("Connect fail!\n");
        lwip_close(sock);

        /*釋放接收緩衝 */
        rt_free(recv_data);
        return;
    }

    while(1)
    {
        /* 從sock連接中接收最大BUFSZ - 1字節數據 */
        bytes_received = recv(sock, recv_data, BUFSZ - 1, 0);
        if (bytes_received <= 0)
        {
            /* 接收失敗，關閉這個連接 */
            lwip_close(sock);

            /* 釋放接收緩衝 */
            rt_free(recv_data);
            break;
        }

        /* 有接收到數據，把末端清零 */
        recv_data[bytes_received] = '\0';

        if (strcmp(recv_data , "q") == 0 || strcmp(recv_data , "Q") == 0)
        {
            /* 如果是首字母是q或Q，關閉這個連接 */
            lwip_close(sock);

            /* 釋放接收緩衝 */
            rt_free(recv_data);
            break;
        }
        else
        {
            /* 在控制終端顯示收到的數據 */
            rt_kprintf("\nReceived data = %s " , recv_data);
        }

        /* 發送數據到sock連接 */
        send(sock,send_data,strlen(send_data), 0);
    }

    return;
}

#ifdef RT_USING_FINSH
#include <finsh.h>
/* 輸出tcpclient函數到finsh shell中 */
FINSH_FUNCTION_EXPORT(tcpclient, startup tcp client);
#endif
