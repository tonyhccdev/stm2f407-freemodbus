#include <rtthread.h>
#include <lwip/netdb.h> /* 為瞭解析主機名，需要包含netdb.h頭文件 */
#include <lwip/sockets.h> /* 使用BSD socket，需要包含sockets.h頭文件 */

void tcp_senddata(const char* url, int port, int length)
{
	struct hostent *host;
	int sock, err, result, timeout, index;
	struct sockaddr_in server_addr;
	rt_uint8_t *buffer_ptr;

	/* 通過函數入口參數url獲得host地址（如果是域名，會做域名解析） */
	host = gethostbyname(url);
	/* 創建一個socket，類型是SOCKET_STREAM，TCP類型 */
	if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{
		/* 創建socket失敗 */
		rt_kprintf("Socket error\n");
		return;
	}

	/* 神奇內存 */
	buffer_ptr = rt_malloc(length);
	/* 構造發生數據 */
	for (index = 0; index < length; index ++)
		buffer_ptr[index] = index & 0xff;

	timeout = 100;
	/* 設置發送超時時間100ms */
	lwip_setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout));
	/* 初始化預連接的服務端地址 */
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr = *((struct in_addr *)host->h_addr);
	rt_memset(&(server_addr.sin_zero), 0, sizeof(server_addr.sin_zero));

	/* 連接到服務端 */
	err = connect(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
	rt_kprintf("TCP thread connect error code: %d\n", err);

	while(1)
	{
		/* 發送數據到sock連接 */
		result = send(sock, buffer_ptr, length, MSG_DONTWAIT);
		if(result == -1) //數據發送錯誤處理
		{
			rt_kprintf("TCP thread send error: %d\n", result);
			lwip_close(sock);	//關閉連接，重新創建連接
			rt_thread_delay(10);
			if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
				rt_kprintf("TCP Socket error:%d\n",sock);
			err = connect(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
			rt_kprintf("TCP thread connect error code: %d\n", err);
		}
	}
}

#ifdef RT_USING_FINSH
#include <finsh.h>
/* 輸出tcpclient函數到finsh shell中 */
FINSH_FUNCTION_EXPORT(tcp_senddata, send a packet through tcp connection);
#endif

