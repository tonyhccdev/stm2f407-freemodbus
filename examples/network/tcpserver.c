#include <rtthread.h>
#include <lwip/sockets.h> /* 使用BSD Socket接口必須包含sockets.h這個頭文件 */

static const char send_data[] = "This is TCP Server from RT-Thread."; /* 發送用到的數據 */
void tcpserv(void* parameter)
{
   char *recv_data; /* 用於接收的指針，後面會做一次動態分配以請求可用內存 */
   rt_uint32_t sin_size;
   int sock, connected, bytes_received;
   struct sockaddr_in server_addr, client_addr;
   rt_bool_t stop = RT_FALSE; /* 停止標誌 */

   recv_data = rt_malloc(1024); /* 分配接收用的數據緩衝 */
   if (recv_data == RT_NULL)
   {
       rt_kprintf("No memory\n");
       return;
   }

   /* 一個socket在使用前，需要預先創建出來，指定SOCK_STREAM為TCP的socket */
   if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
   {
       /* 創建失敗的錯誤處理 */
       rt_kprintf("Socket error\n");

       /* 釋放已分配的接收緩衝 */
       rt_free(recv_data);
       return;
   }

   /* 初始化服務端地址 */
   server_addr.sin_family = AF_INET;
   server_addr.sin_port = htons(5000); /* 服務端工作的端口 */
   server_addr.sin_addr.s_addr = INADDR_ANY;
   rt_memset(&(server_addr.sin_zero),8, sizeof(server_addr.sin_zero));

   /* 綁定socket到服務端地址 */
   if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
   {
       /* 綁定失敗 */
       rt_kprintf("Unable to bind\n");

       /* 釋放已分配的接收緩衝 */
       rt_free(recv_data);
       return;
   }

   /* 在socket上進行監聽 */
   if (listen(sock, 5) == -1)
   {
       rt_kprintf("Listen error\n");

       /* release recv buffer */
       rt_free(recv_data);
       return;
   }

   rt_kprintf("\nTCPServer Waiting for client on port 5000...\n");
   while(stop != RT_TRUE)
   {
       sin_size = sizeof(struct sockaddr_in);

       /* 接受一個客戶端連接socket的請求，這個函數調用是阻塞式的 */
       connected = accept(sock, (struct sockaddr *)&client_addr, &sin_size);
       /* 返回的是連接成功的socket */

       /* 接受返回的client_addr指向了客戶端的地址信息 */
       rt_kprintf("I got a connection from (%s , %d)\n",
                  inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));

       /* 客戶端連接的處理 */
       while (1)
       {
           /* 發送數據到connected socket */
           send(connected, send_data, strlen(send_data), 0);

           /* 從connected socket中接收數據，接收buffer是1024大小，但並不一定能夠收到1024大小的數據 */
           bytes_received = recv(connected,recv_data, 1024, 0);
           if (bytes_received <= 0)
           {
               /* 接收失敗，關閉這個connected socket */
               lwip_close(connected);
               break;
           }

           /* 有接收到數據，把末端清零 */
           recv_data[bytes_received] = '\0';
           if (strcmp(recv_data , "q") == 0 || strcmp(recv_data , "Q") == 0)
           {
               /* 如果是首字母是q或Q，關閉這個連接 */
               lwip_close(connected);
               break;
           }
           else if (strcmp(recv_data, "exit") == 0)
           {
               /* 如果接收的是exit，則關閉整個服務端 */
               lwip_close(connected);
               stop = RT_TRUE;
               break;
           }
           else
           {
               /* 在控制終端顯示收到的數據 */
               rt_kprintf("RECEIVED DATA = %s \n" , recv_data);
           }
       }
   }

   /* 退出服務 */
   lwip_close(sock);

   /* 釋放接收緩衝 */
   rt_free(recv_data);

   return ;
}

#ifdef RT_USING_FINSH
#include <finsh.h>
/* 輸出tcpserv函數到finsh shell中 */
FINSH_FUNCTION_EXPORT(tcpserv, startup tcp server);
#endif
